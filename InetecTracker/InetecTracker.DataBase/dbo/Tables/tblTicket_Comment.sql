﻿CREATE TABLE [dbo].[tblTicket_Comment]
(
	[Id]	INT		IDENTITY (1, 1) NOT NULL,
	[Ticket] INT not NULL, 
	[Comment] NVARCHAR(max) not null,
	[CreatedBy] int not null,
	[CreatedDate] DateTime not null default(getdate()),
    CONSTRAINT [PK_Ticket_Comment] PRIMARY KEY CLUSTERED ([Id] ASC),
	CONSTRAINT [FK_Ticket_Comment] FOREIGN KEY ([Ticket]) REFERENCES [dbo].[tblTicket] ([Id]) ON DELETE CASCADE,
	CONSTRAINT [FK_Ticket_Comment_CreatedBy] FOREIGN KEY ([CreatedBy]) REFERENCES [dbo].[tblAccount] ([Id]),
)
