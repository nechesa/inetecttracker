﻿CREATE TABLE [dbo].[tblProject]
(
	[Id] INT IDENTITY (1, 1) NOT NULL,
	[Title] NVARCHAR(MAX) NOT NULL, 
    [Description] NVARCHAR(MAX) NULL, 
    [CreatedBy] INT NOT NULL, 
	[CreatedDate] DateTime not null DEFAULT (getdate()),
    CONSTRAINT [PK_Project] PRIMARY KEY CLUSTERED ([Id] ASC),
	CONSTRAINT [FK_Project_Account] FOREIGN KEY ([CreatedBy]) REFERENCES [dbo].[tblAccount] ([Id]) 
)
