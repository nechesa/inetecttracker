﻿CREATE TABLE [dbo].[tblProject_Member]
(
	[Project] INT NOT NULL, 
    [Account] INT NOT NULL,
	CONSTRAINT [PK_Project_Member] PRIMARY KEY CLUSTERED ([Project], [Account] ASC),
	CONSTRAINT [FK_Project_Member_Project] FOREIGN KEY ([Project]) REFERENCES [dbo].[tblProject] ([Id]) ON DELETE CASCADE,
	CONSTRAINT [FK_Project_Member_Account] FOREIGN KEY ([Account]) REFERENCES [dbo].[tblAccount] ([Id]) ON DELETE CASCADE
)
