﻿CREATE TABLE [dbo].[tblTicket]
(
	[Id]	INT		IDENTITY (1, 1) NOT NULL,
	[Project] INT not NULL, 
    [Title] NVARCHAR(MAX) NOT NULL, 
    [Description] NVARCHAR(MAX) NULL, 
	[Priority] INT not null,
	[Type] Int not null,
	[Status] Int not null,
	[CreatedBy] int not null,
	[CreatedDate] DateTime not null default(getdate()),
	[AssignTo] int not null,
    CONSTRAINT [PK_Ticket] PRIMARY KEY CLUSTERED ([Id] ASC),
	CONSTRAINT [FK_Ticket_Project] FOREIGN KEY ([Project]) REFERENCES [dbo].[tblProject] ([Id]) ON DELETE CASCADE,
	CONSTRAINT [FK_CreatedBy] FOREIGN KEY ([CreatedBy]) REFERENCES [dbo].[tblAccount] ([Id]),
	CONSTRAINT [FK_AssigenedTo] FOREIGN KEY ([AssignTo]) REFERENCES [dbo].[tblAccount] ([Id]) 
)
