﻿CREATE TABLE [dbo].[tblAccount] (
    [Id]           INT            IDENTITY (1, 1) NOT NULL,
    [AccountType]  INT            NOT NULL,
    [Email]        NVARCHAR (256) NOT NULL,
    [FullName]     NVARCHAR (256) NOT NULL,
    [IsApproved]   BIT            NOT NULL,
    [PasswordHash] NVARCHAR (256) NOT NULL,
    [PasswordSalt] NVARCHAR (256) NOT NULL,
    [IsLockedOut]  BIT            NOT NULL,
	[IsDeleted]    BIT            NOT NULL,
    [CretaeDate]   DATETIME       NOT NULL,
    CONSTRAINT [PK_Account] PRIMARY KEY CLUSTERED ([Id] ASC)
);

