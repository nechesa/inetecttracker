﻿using InetecTracker.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InetecTracker.Infrastructure.Interfaces
{
    public interface IProjectMembershipService
    {
        bool IsUserMemberOfProject(int userId, int projectId);
        void AddUserToProject(int userId, int projectId);
        void RemoveUserFromProject(int userId, int projectId);

        List<IdName> GetProjectUsers(int projectId);
        void RemoveAllUsersFromProject(int projectId);
    }
}
