﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InetecTracker.Infrastructure.Interfaces
{
    public interface IAdminService: IAccountCRUD, IProjectCRUD, ITicketCRUD, IProjectMembershipService
    {
    }
}
