﻿using InetecTracker.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InetecTracker.Infrastructure.Interfaces
{
    public interface IAccountCRUD
    {
        List<AccountModel> GetAccounts();
        AccountModel GetAccount(int id);
        int UpdateAccount(AccountModel model);
        void DeleteAccount(int id);
    }
}
