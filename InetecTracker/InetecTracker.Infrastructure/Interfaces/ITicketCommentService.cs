﻿using InetecTracker.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InetecTracker.Infrastructure.Interfaces
{
    public interface ITicketCommentService
    {

        bool IsCommentAvailable(int ticketId, int userId);

        List<TicketCommentModel> GetTicketComments(int ticketId);


        int AddTicketComment(TicketCommentModel model);
        void RemoveTicketComment(int id);
    }
}
