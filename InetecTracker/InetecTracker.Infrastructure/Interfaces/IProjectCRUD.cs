﻿using InetecTracker.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InetecTracker.Infrastructure.Interfaces
{
    public interface IProjectCRUD
    {
        List<ProjectModel> GetProjects();
        ProjectModel GetProject(int id);        
        int UpdateProject(ProjectModel model);
        void DeleteProject(int id);
    }
}
