﻿using InetecTracker.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InetecTracker.Infrastructure.Interfaces
{
    public interface ITicketCRUD
    {
        List<TicketModel> GetTickets(int projectId);
        TicketModel GetTicket(int id);
        int UpdateTicket(TicketModel model);
        void DeleteTicket(int id);
    }
}
