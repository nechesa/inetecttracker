﻿using InetecTracker.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InetecTracker.Infrastructure.Interfaces
{
    public interface IAccountService
    {
        List<AccountModel> GetAllAccounts();

        bool IsAccountExist(int id);

        bool IsEmailExist(string email);

        bool IsAccountActive(string email);

        bool IsAccountLockedOrDeleted(string email);

        int GetAccountRole(string email);

        int GetAccountId(string email);

        string GetAccountName(int id);

        void CreateAccount(AccountModel model, string password);

        bool IsPasswordValid(LoginModel model);

        void ChangePassword(string newPassword, int accountId);

        void SignIn(LoginModel model, out int role);

        void SignOut();

        #region password encryption

        string EncodePassword(string password, string salt);

        string GenerateSalt();

        #endregion
    }
}
