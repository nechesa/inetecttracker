﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace InetecTracker.Infrastructure.Attributes
{
    public class CustomEmailAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            var str = value as string;
            if (string.IsNullOrEmpty(str))
                return true;
            str = str.Trim();
            //if email contains spaces
            const string spaceRegexp = @"\s";
            var spacePattern = new Regex(spaceRegexp);
            if (spacePattern.IsMatch(str))
                return false;

            //if email is email
            const string regexp =
                @"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?";
            var pattern = new Regex(regexp);
            return pattern.IsMatch(str);
        }
    }
}
