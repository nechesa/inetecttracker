﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InetecTracker.Infrastructure
{
    public enum AccountType
    {
        Anonymous = -1,
        Admin = 0,
        Developer = 1,
        User = 2
    }

    public enum TicketType
    {
        
        Bug = 1,
        Feature = 2,
        Task = 3,
        //etc....
    }

    public enum PriorityType
    {
        None = 0,
        Low = 1,
        Medium = 2,
        Hight = 3,
        Critical = 4
    }

    public enum StatusType
    {
        None = 0,
        Open = 1,
        InProgress = 2,
        Resolved = 3

       //etc...
       //deleted
       //arhived
    }
}
