﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InetecTracker.Infrastructure.Models
{
    public class TicketModel : IdName
    {

        public TicketModel()
        {
            AssignTo = new IdName();
        }

        [Required(ErrorMessage = "*")]
        [MinLength(4, ErrorMessage = "Name must be more than 3 characters")]
        public string Name { get; set; }

        [Required(ErrorMessage = "*")]
        public int ProjectId { get; set; }

        public string Description { get; set; }


        [Required(ErrorMessage = "*")]
        public int Priority { get; set; }
        [Required(ErrorMessage = "*")]
        public int Type { get; set; }
        [Required(ErrorMessage = "*")]
        public int Status { get; set; }

        public DateTime CreatedDate { get; set; }
        public IdName CreatedBy { get; set; }
        public IdName AssignTo { get; set; }
    }
}
