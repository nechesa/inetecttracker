﻿using InetecTracker.Infrastructure.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InetecTracker.Infrastructure.Models
{
    public class AccountModel
    {
        public int Id { get; set; }
        public int AccountType { get; set; }
        public bool IsApproved { get; set; }
        public bool IsLockedOut { get; set; }
        public bool IsDeleted { get; set; }

        [Required(ErrorMessage = "*")]
        [CustomEmail(ErrorMessage = "Invalid Email")]
        [DataType(DataType.EmailAddress, ErrorMessage = "*")]
        public string Email { get; set; }

        [Required(ErrorMessage = "*")]
        [MinLength(3, ErrorMessage = "Name must be more than 3 characters")]
        public string FullName { get; set; }


        public string PasswordSalt { get; set; }
        public string PasswordHash { get; set; }

    }
}
