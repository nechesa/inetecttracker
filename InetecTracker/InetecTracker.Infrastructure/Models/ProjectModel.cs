﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InetecTracker.Infrastructure.Models
{
    public class ProjectModel : IdName
    {
        [Required(ErrorMessage = "*")]
        [MinLength(4, ErrorMessage = "Project Name must be more than 3 characters")]
        public string Name { get; set; }

        public string Description { get; set; }
        public DateTime CreatedDate { get; set; }
        public IdName CreatedBy { get; set; }
        public List<IdName> ProjectMembers { get; set; }
    }
}
