﻿using InetecTracker.Infrastructure.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InetecTracker.Infrastructure.Models
{
    public class RegistrationModel
    {
        [Required(ErrorMessage = "*")]
        [Display(Name = "name")]
        [MinLength(3, ErrorMessage = "Name must be more than 3 characters")]
        public string Name { get; set; }


        [Required(ErrorMessage = "*")]
        [CustomEmail(ErrorMessage = "Invalid Email")]
        [DataType(DataType.EmailAddress, ErrorMessage = "*")]
        [Display(Name = "email")]
        public string Email { get; set; }

        [Required(ErrorMessage = "*")]
        [Display(Name = "type")]
        public AccountType AccountType { get; set; }

        [Required(ErrorMessage = "*")]
        [MinLength(4, ErrorMessage = "Password must be more than 3 characters")]
        [Display(Name = "password")]
        public string Password { get; set; }

        [Required(ErrorMessage = "*")]
        [MinLength(4, ErrorMessage = "Password must be more than 3 characters")]
        [Display(Name = "password")]
        public string PasswordConfirm { get; set; }
    }
}
