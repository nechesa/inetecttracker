﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InetecTracker.Infrastructure.Models
{
    public class IdName
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class IdNameGroup : IdName
    {
        public int? Group { get; set; }
    }

    public class IdNamePath : IdName
    {
        public string Path { get; set; }
    }

    public class IdNameBool : IdName
    {
        public bool Bool { get; set; }
        
    }
}
