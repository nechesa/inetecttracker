﻿using InetecTracker.Infrastructure.Interfaces;
using InetecTracker.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace InetecTracker.Controllers
{
    [Authorize]
    public class ControlController : Controller
    {


        private readonly ITicketCommentService commentService = null;
        private readonly IAccountService accountService = null;


        public ControlController(ITicketCommentService CommentService, IAccountService AccountService)
        {
            commentService = CommentService;
            accountService = AccountService;
        }

        private int currentUserId()
        {
            var currentUser = HttpContext.User.Identity.Name;
            return accountService.GetAccountId(currentUser);
        }

        public ActionResult TicketCommentControl(int ticketId)
        {
            int currentId = currentUserId();
            ViewBag.isAvaliable = commentService.IsCommentAvailable(ticketId, currentId);
            return PartialView("TicketCommentControl", ticketId);
        }

        public ActionResult TicketCommentList(int ticketid)
        {
            List<TicketCommentModel> model = commentService.GetTicketComments(ticketid);
            return PartialView("TicketCommentList", model);
        }

        public ActionResult TicketCommentForm(int ticketId)
        {
            TicketCommentModel model = new TicketCommentModel { TicketId = ticketId };
            return PartialView("TicketCommentForm", model);
        }

        [HttpPost]
        public ActionResult AddComment(int ticketId, string comment)
        {
            if (!String.IsNullOrWhiteSpace(comment))
            {
                TicketCommentModel model = new TicketCommentModel();
                model.Comment = comment;
                model.TicketId = ticketId;
                model.CreateBy = new IdName { Id = currentUserId(), Name = String.Empty };

                commentService.AddTicketComment(model);
            }
            return TicketCommentList(ticketId);
        }
    }
}