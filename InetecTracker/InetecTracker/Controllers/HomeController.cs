﻿using InetecTracker.Infrastructure;
using InetecTracker.Infrastructure.Interfaces;
using InetecTracker.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace InetecTracker.Controllers
{
    public class HomeController : Controller
    {
        private readonly IAccountService accountService = null;

        public HomeController(IAccountService AccountService)
        {
            accountService = AccountService;
        }

        [HttpGet]
        public ActionResult Index()
        {
            return View("Index");
        }
        [HttpPost]
        public ActionResult Index(LoginModel model)
        {
            if (ModelState.IsValid)
            {
                if (accountService.IsEmailExist(model.Email))
                {
                    if (accountService.IsPasswordValid(model) && accountService.IsAccountActive(model.Email))
                    {
                        int role;
                        accountService.SignIn(model, out role);
                        return RedirectToAction("Index", "Home", new { area = String.Empty });
                    }
                    else
                    {
                        ModelState.AddModelError("Password", "Password is incorrect or account isn't active");
                    }
                }
                else
                {
                    ModelState.AddModelError("Email", "Invalid Email");
                }
            }
            return View(model);
        }

        public ActionResult SignOut()
        {
            accountService.SignOut();
            return RedirectToAction("Index", "Home", new { area = String.Empty });
        }

        [HttpGet]
        public ActionResult Registration()
        {
            RegistrationModel model = new RegistrationModel();
            return View("Registration", model);
        }

        [HttpPost]
        public ActionResult Registration(RegistrationModel model)
        {
            if (ModelState.IsValid)
            {
                if (model.Password == model.PasswordConfirm)
                {
                    if (!accountService.IsEmailExist(model.Email))
                    {
                        accountService.SignOut();
                        accountService.CreateAccount(new AccountModel
                        {
                            AccountType = (int)model.AccountType,
                            FullName = model.Name,
                            Email = model.Email,
                            IsApproved = false,
                        }, model.Password);

                        return RedirectToAction("Index", "Home", new { area = String.Empty });
                    }
                    else
                    {
                        ModelState.AddModelError("Email", "This email already registered");
                    }
                }
                else
                {
                    ModelState.AddModelError("PasswordConfirm", "Password and Password Confirm does not equals");
                }
            }

            return View("Registration", model);
        }
    }
}