﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Autofac;
using Autofac.Integration.Mvc;
using InetecTracker.Infrastructure.Interfaces;
using InetecTracker.Services;
using InetecTracker.Services.Admin;
using InetecTracker.Services.Base;
using InetecTracker.Services.Developer;
using InetecTracker.Services.User;

namespace InetecTracker
{
    public class AutofacConfig
    {
        public static void ConfigureContainer()
        {
            var builder = new ContainerBuilder();
            builder.RegisterControllers(typeof(MvcApplication).Assembly);


            // Registering types
            builder.RegisterType<AccountService>().As<IAccountService>();

            builder.RegisterType<AdminService>().As<IAdminService>();

            builder.RegisterType<DeveloperService>().As<IDeveloperService>();

            builder.RegisterType<UserService>().As<IUserService>();

            builder.RegisterType<TicketCommentService>().As<ITicketCommentService>();

            

            var container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }
    }
}