﻿using InetecTracker.Infrastructure.Interfaces;
using InetecTracker.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace InetecTracker.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class HomeController : Controller
    {
        private readonly IAdminService adminService = null;
        private readonly IAccountService accountService = null;

        public HomeController(IAdminService AdminService, IAccountService AccountService)
        {
            adminService = AdminService;
            accountService = AccountService;
        }

        #region Account
        public ActionResult Accounts()
        {
            List<AccountModel> model = adminService.GetAccounts();
            return View("Accounts", model);
        }

        [HttpGet]
        public ActionResult EditAccount(int id)
        {
            AccountModel model = adminService.GetAccount(id);
            return View("EditAccount", model);
        }


        [HttpPost]
        public ActionResult EditAccount(AccountModel model, string newPassword)
        {
            ModelState.Remove("Email");
            if (!String.IsNullOrWhiteSpace(newPassword) && newPassword.Length < 4)
            {
                ModelState.AddModelError("newPassword", "New password too short");
            }

            if (ModelState.IsValid)
            {
                adminService.UpdateAccount(model);
                if (!String.IsNullOrWhiteSpace(newPassword) && newPassword.Length > 4)
                {
                    accountService.ChangePassword(newPassword, model.Id);
                }
                return Accounts();
            }
            return View("EditAccount", model);
        }

        #endregion;

        #region Project

        public ActionResult Projects()
        {
            List<ProjectModel> model = adminService.GetProjects();
            return View("Projects", model);
        }


        private List<IdNameBool> InitProjectMembersViewBag(List<IdName> model)
        {
            if (model == null)
            {
                model = new List<IdName>();
            }

            List<int> members = model.Select(x => x.Id).ToList();
            return adminService.GetAccounts()
                 .Select(x => new IdNameBool
                 {
                     Id = x.Id,
                     Name = String.Format("{0} : {1}", x.Email, x.FullName),
                     Bool = members.Contains(x.Id)
                 })
             .ToList();
        }

        private void SaveProjectMembers(int projectId, int[] members)
        {
            adminService.RemoveAllUsersFromProject(projectId);
            if (members != null && members.Any())
            {
                foreach (var i in members)
                {
                    adminService.AddUserToProject(i, projectId);
                }
            }
        }

        [HttpGet]
        public ActionResult EditProject(int id)
        {
            ProjectModel model = adminService.GetProject(id);

            model.ProjectMembers = adminService.GetProjectUsers(id);

            ViewBag.userList = InitProjectMembersViewBag(model.ProjectMembers);

            return View("EditProject", model);
        }

        [HttpPost]
        public ActionResult EditProject(ProjectModel model, int[] members)
        {
            if (ModelState.IsValid)
            {
                if (model.Id == 0)
                {
                    var currentUser = HttpContext.User.Identity.Name;
                    model.CreatedBy = new IdName { Id = accountService.GetAccountId(currentUser), Name = currentUser };
                }

                model.Id = adminService.UpdateProject(model);

                SaveProjectMembers(model.Id, members);

                return RedirectToAction("Projects");
            }


            ViewBag.userList = InitProjectMembersViewBag(model.ProjectMembers);
            return View("EditProject", model);
        }


        public ActionResult RemoveProject(int id)
        {
            adminService.DeleteProject(id);
            return RedirectToAction("Projects");
        }

        #endregion;


        #region ticket

        private List<IdName> InitProjectListViewBag()
        {
            return adminService.GetProjects()
                .Select(x => new IdName { Id = x.Id, Name = x.Name })
                .ToList();
        }

        private List<IdNameBool> InitUserListViewBag(int userId)
        {
            return adminService.GetAccounts()
                 .Select(x => new IdNameBool
                 {
                     Id = x.Id,
                     Name = String.Format("{0} : {1}", x.Email, x.FullName),
                     Bool = userId == x.Id
                 })
             .ToList();
        }

        public ActionResult Tickets(int? projectId)
        {
            ViewBag.projectList = InitProjectListViewBag();
            return View("Tickets", projectId.GetValueOrDefault(0));
        }

        public ActionResult TicketList(int projectId, int? skip, int? take)
        {
            List<TicketModel> model = adminService.GetTickets(projectId);


            if (skip.HasValue)
            {
                model = model.Skip(skip.Value).ToList();
            }

            if (take.HasValue)
            {
                model = model.Take(take.Value).ToList();
            }


            return PartialView("TicketList", model);
        }

        [HttpGet]
        public ActionResult EditTicket(int id, int? projectId)
        {
            TicketModel model = adminService.GetTicket(id);

            if (projectId.HasValue)
            {
                model.ProjectId = projectId.Value;
            }

            ViewBag.userList = InitUserListViewBag(model.AssignTo == null ? 0 : model.AssignTo.Id);
            ViewBag.projectList = InitProjectListViewBag();
            return View("EditTicket", model);
        }

        [HttpPost]
        public ActionResult EditTicket(TicketModel model)
        {
            if (model.AssignTo == null ||  model.AssignTo.Id == 0)
            {
                ModelState.AddModelError("AssignTo", "Ticket should be assigened to user");
            }

            if (ModelState.IsValid)
            {
                if (model.Id == 0)
                {
                    var currentUser = HttpContext.User.Identity.Name;
                    model.CreatedBy = new IdName { Id = accountService.GetAccountId(currentUser), Name = currentUser };
                }
                
                adminService.UpdateTicket(model);
                return RedirectToAction("Tickets", new { projectId = model.ProjectId });
            }

            ViewBag.userList = InitUserListViewBag(model.AssignTo == null ? 0 : model.AssignTo.Id);
            ViewBag.projectList = InitProjectListViewBag();
            return View("EditTicket", model);
        }

        public ActionResult RemoveTicket(int id)
        {
            var ticket = adminService.GetTicket(id);
            if (ticket.Id != 0)
            {
                adminService.DeleteTicket(id);
            }
            return RedirectToAction("Tickets", new { projectId = ticket.ProjectId});
        }

        #endregion;
    }
}