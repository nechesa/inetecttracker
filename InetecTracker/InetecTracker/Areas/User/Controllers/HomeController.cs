﻿using InetecTracker.Infrastructure.Interfaces;
using InetecTracker.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace InetecTracker.Areas.User.Controllers
{
    [Authorize(Roles = "User")]
    public class HomeController : Controller
    {
        private readonly IUserService userService = null;
        private readonly IAccountService accountService = null;


        public HomeController(IUserService UserService, IAccountService AccountService)
        {
            userService = UserService;
            accountService = AccountService;
        }

        private int currentUserId()
        {
            var currentUser = HttpContext.User.Identity.Name;
            return accountService.GetAccountId(currentUser);
        }


        #region ticket

        private List<IdName> InitProjectListViewBag()
        {
            
            return userService.GetProjects(currentUserId());
        }

        private List<IdNameBool> InitUserListViewBag(int projectId, int userId)
        {
            return userService.GetAccountsByProject(projectId)
                 .Select(x => new IdNameBool
                 {
                     Id = x.Id,
                     Name = x.Name,
                     Bool = userId == x.Id
                 })
             .ToList();
        }

        public ActionResult Tickets(int? projectId)
        {
            ViewBag.projectList = InitProjectListViewBag();
            return View("Tickets", projectId.GetValueOrDefault(0));
        }

        public ActionResult TicketList(int projectId, int? skip, int? take)
        {
            int currentId = currentUserId();
            List<TicketModel> model = userService.GetTickets(projectId)
                .Where(x=>x.CreatedBy.Id == currentId || x.AssignTo.Id == currentId)
                .ToList();

            if (skip.HasValue)
            {
                model = model.Skip(skip.Value).ToList();
            }

            if (take.HasValue)
            {
                model = model.Take(take.Value).ToList();
            }


            return PartialView("TicketList", model);
        }

        [HttpGet]
        public ActionResult EditTicket(int id, int? projectId)
        {
            TicketModel model = userService.GetTicket(id);

            //business logic: User can view ticket that was created / assigened for him, or create new one 
            int currentId = currentUserId();
            if (model.Id != 0 && model.CreatedBy.Id != currentId && model.AssignTo.Id != currentId)
            {
                model = new TicketModel();
            }
            //

            if (projectId.HasValue)
            {
                model.ProjectId = projectId.Value;
            }

            ViewBag.userList = InitUserListViewBag(model.ProjectId, model.AssignTo == null ? 0 : model.AssignTo.Id);
            return View("EditTicket", model);
        }

        [HttpPost]
        public ActionResult EditTicket(TicketModel model)
        {
            if (model.AssignTo == null || model.AssignTo.Id == 0)
            {
                ModelState.AddModelError("AssignTo", "Ticket should be assigened to user");
            }

            if (ModelState.IsValid)
            {
                if (model.Id == 0)
                {
                    var currentUser = HttpContext.User.Identity.Name;
                    model.CreatedBy = new IdName { Id = accountService.GetAccountId(currentUser), Name = currentUser };
                    userService.UpdateTicket(model);
                }

                return RedirectToAction("Tickets", new { projectId = model.ProjectId });
            }

            ViewBag.userList = InitUserListViewBag(model.ProjectId, model.AssignTo == null ? 0 : model.AssignTo.Id);
            return View("EditTicket", model);
        }

        #endregion;

    }
}