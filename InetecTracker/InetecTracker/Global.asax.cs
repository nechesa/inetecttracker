﻿using InetecTracker.Infrastructure;
using InetecTracker.Infrastructure.Interfaces;
using InetecTracker.Infrastructure.Models;
using InetecTracker.Services.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace InetecTracker
{
    public class MvcApplication : System.Web.HttpApplication
    {
        private IAccountService accountService;

        public void Application_AuthenticateRequest(Object src, EventArgs e)
        {
            if (!(HttpContext.Current.User == null))
            {
                if (HttpContext.Current.User.Identity.AuthenticationType == "Forms")
                {
                    System.Web.Security.FormsIdentity id;
                    id = (System.Web.Security.FormsIdentity)HttpContext.Current.User.Identity;
                    String[] myRoles = new String[1];
                    if (id != null && !String.IsNullOrWhiteSpace(id.Name))
                    {
                        accountService = DependencyResolver.Current.GetService<IAccountService>();

                        AccountType role = Context.Cache
                        .GetOrStore<AccountType>("userCachKey" + id.Name,
                            () => (AccountType)accountService.GetAccountRole(id.Name), 2);
                            myRoles[0] = role.ToString();
                    }

                    HttpContext.Current.User = new System.Security.Principal.GenericPrincipal(id, myRoles);
                }
            }
        }

        protected void Application_Start()
        {
            AutofacConfig.ConfigureContainer();


            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);


            //Used for created test admin
            InitAdministrators();


            #if DEBUG
            //used for creating test users not for production only for debuging
            InitTestUsers();
            #endif
        }


        private void InitAdministrators()
        {
            using (AccountService accountService = new AccountService())
            {
                AccountModel adminAccount = new AccountModel()
                {
                    FullName = "Admin",
                    Email = "nechesa2010@gmail.com",
                    AccountType = (int)AccountType.Admin,
                    IsApproved = true
                };

                if (!accountService.IsEmailExist(adminAccount.Email))
                {
                    string password = "1111";
                    accountService
                        .CreateAccount(adminAccount, password);
                }


                AccountModel devAccount = new AccountModel()
                {
                    FullName = "Developer",
                    Email = "dev@gmail.com",
                    AccountType = (int)AccountType.Developer,
                    IsApproved = true
                };

                if (!accountService.IsEmailExist(devAccount.Email))
                {
                    string password = "1111";
                    accountService
                        .CreateAccount(devAccount, password);
                }


                AccountModel userAccount = new AccountModel()
                {
                    FullName = "User",
                    Email = "user@gmail.com",
                    AccountType = (int)AccountType.User,
                    IsApproved = true
                };

                if (!accountService.IsEmailExist(userAccount.Email))
                {
                    string password = "1111";
                    accountService
                        .CreateAccount(userAccount, password);
                }
            }
        }

        private void InitTestUsers()
        {
            using (AccountService accountService = new AccountService())
            {
                AccountModel devAccount = new AccountModel()
                {
                    FullName = "Developer",
                    Email = "dev@gmail.com",
                    AccountType = (int)AccountType.Developer,
                    IsApproved = true
                };

                if (!accountService.IsEmailExist(devAccount.Email))
                {
                    string password = "1111";
                    accountService
                        .CreateAccount(devAccount, password);
                }


                AccountModel userAccount = new AccountModel()
                {
                    FullName = "User",
                    Email = "user@gmail.com",
                    AccountType = (int)AccountType.User,
                    IsApproved = true
                };

                if (!accountService.IsEmailExist(userAccount.Email))
                {
                    string password = "1111";
                    accountService
                        .CreateAccount(userAccount, password);
                }
            }
        }




    }
}
