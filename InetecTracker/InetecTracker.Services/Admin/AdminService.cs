﻿using InetecTracker.Infrastructure.Interfaces;
using InetecTracker.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InetecTracker.Services.Admin
{
    public class AdminService : Base.BaseService, IAdminService
    {
        #region Account CRUD
        public void DeleteAccount(int id)
        {
            using (var ctx = CreateContext())
            {
                var item = ctx.tblAccounts.FirstOrDefault(x => x.Id == id);
                if (item != null)
                {
                    item.IsDeleted = true;
                    ctx.SubmitChanges();
                }
            }
        }
        public AccountModel GetAccount(int id)
        {
            using (var ctx = CreateContext())
            {
                var item = ctx.tblAccounts.FirstOrDefault(x => x.Id == id);
                if (item != null)
                {
                    return new AccountModel
                    {
                        Id = item.Id,
                        AccountType = item.AccountType,
                        Email = item.Email,
                        FullName = item.FullName,
                        IsApproved = item.IsApproved,
                        IsLockedOut = item.IsLockedOut,
                        IsDeleted = item.IsDeleted,
                        PasswordHash = item.PasswordHash,
                        PasswordSalt = item.PasswordSalt
                    };
                }

                return null;
            }
        }
        public List<AccountModel> GetAccounts()
        {
            using (var ctx = CreateContext())
            {
                return ctx.tblAccounts.Select(x => new AccountModel
                {
                    Id = x.Id,
                    AccountType = x.AccountType,
                    Email = x.Email,
                    FullName = x.FullName,
                    IsApproved = x.IsApproved,
                    IsLockedOut = x.IsLockedOut,
                    IsDeleted = x.IsDeleted,
                    PasswordHash = x.PasswordHash,
                    PasswordSalt = x.PasswordSalt
                }).ToList();
            }
        }
        public int UpdateAccount(AccountModel model)
        {
            using (var ctx = CreateContext())
            {
                var item = ctx.tblAccounts.FirstOrDefault(x => x.Id == model.Id);
                if (item == null)
                {
                    item = new Data.tblAccount();
                    item.Email = model.Email;
                    item.PasswordHash = model.PasswordHash;
                    item.PasswordSalt = model.PasswordSalt;
                    item.CretaeDate = DateTime.Now;

                    ctx.tblAccounts.InsertOnSubmit(item);
                }

                item.FullName = model.FullName;
                item.AccountType = model.AccountType;
                item.IsApproved = model.IsApproved;
                item.IsLockedOut = model.IsLockedOut;
                item.IsDeleted = model.IsDeleted;

                ctx.SubmitChanges();
                return item.Id;
            }
        }

        #endregion;

        #region Project CRUD
        public ProjectModel GetProject(int id)
        {
            using (var ctx = CreateContext())
            {
                var item = ctx.tblProjects.FirstOrDefault(x => x.Id == id);
                if (item != null)
                {
                    var account = ctx.tblAccounts
                        .FirstOrDefault(x => x.Id == item.CreatedBy);
                    return new ProjectModel
                    {
                        Id = item.Id,
                        Name = item.Title,
                        Description = item.Description,
                        CreatedBy = account == null ? null : new IdName { Id = account.Id, Name = account.FullName },
                        CreatedDate = item.CreatedDate,
                        ProjectMembers = null
                    };
                }
                return new ProjectModel();
            }
        }
        public List<ProjectModel> GetProjects()
        {
            using (var ctx = CreateContext())
            {
                return ctx.tblProjects
                     .Join(ctx.tblAccounts, o => o.CreatedBy, i => i.Id, (o, i) => new { P = o, A = new IdName { Id = i.Id, Name = i.FullName } })
                     .Select(x => new ProjectModel
                     {
                         Id = x.P.Id,
                         Name = x.P.Title,
                         Description = x.P.Description,
                         CreatedDate = x.P.CreatedDate,
                         CreatedBy = x.A,
                         ProjectMembers = null
                     }).ToList();
            }
        }
        public int UpdateProject(ProjectModel model)
        {
            using (var ctx = CreateContext())
            {
                var item = ctx.tblProjects.FirstOrDefault(x => x.Id == model.Id);
                if (item == null)
                {
                    item = new Data.tblProject();
                    item.CreatedDate = DateTime.Now;
                    item.CreatedBy = model.CreatedBy.Id;
                    ctx.tblProjects.InsertOnSubmit(item);
                }

                item.Title = model.Name;
                item.Description = model.Description;

                ctx.SubmitChanges();
                return item.Id;
            }
        }
        public void DeleteProject(int id)
        {
            using (var ctx = CreateContext())
            {
                var item = ctx.tblProjects.FirstOrDefault(x => x.Id == id);
                if (item != null)
                {
                    ctx.tblProjects.DeleteOnSubmit(item);
                    ctx.SubmitChanges();
                }
            }
        }
        #endregion;

        #region Membership
        public bool IsUserMemberOfProject(int userId, int projectId)
        {
            using (var ctx = CreateContext())
            {
                return ctx.tblProject_Members.Any(x => x.Project == projectId && x.Account == userId);
            }
        }

        public void AddUserToProject(int userId, int projectId)
        {
            using (var ctx = CreateContext())
            {
                var item = ctx.tblProject_Members.FirstOrDefault(x => x.Project == projectId && x.Account == userId);
                if (item == null)
                {
                    ctx.tblProject_Members.InsertOnSubmit(new Data.tblProject_Member { Account = userId, Project = projectId });
                    ctx.SubmitChanges();
                }
            }
        }

        public void RemoveUserFromProject(int userId, int projectId)
        {
            using (var ctx = CreateContext())
            {
                var item = ctx.tblProject_Members.FirstOrDefault(x => x.Project == projectId && x.Account == userId);
                if (item != null)
                {
                    ctx.tblProject_Members.DeleteOnSubmit(item);
                    ctx.SubmitChanges();
                }
            }
        }

        public List<IdName> GetProjectUsers(int projectId)
        {
            using (var ctx = CreateContext())
            {
               return ctx.tblProject_Members
                    .Where(x => x.Project == projectId)
                    .Join(ctx.tblAccounts, o => o.Account, i => i.Id, (o, i) => i)
                    .Select(x => new IdName { Id = x.Id, Name = x.FullName })
                    .ToList();
            }
        }

        public void RemoveAllUsersFromProject(int projectId)
        {
            using (var ctx = CreateContext())
            {
                var items = ctx.tblProject_Members.Where(x => x.Project == projectId);
                if (items != null && items.Any())
                {
                    ctx.tblProject_Members.DeleteAllOnSubmit(items);
                    ctx.SubmitChanges();
                }
            }
        }        
        #endregion;

        #region Ticket CRUD
        public List<TicketModel> GetTickets(int projectId)
        {
            using (var ctx = CreateContext())
            {
                return ctx.tblTickets
                    .Where(x=>x.Project == projectId)
                     .Join(ctx.tblAccounts, o => o.CreatedBy, i => i.Id, (o, i) => new { T = o, CB = new IdName { Id = i.Id, Name = i.FullName } })
                     .Join(ctx.tblAccounts, o => o.T.AssignTo, i => i.Id, (o, i) => new { T = o.T, CB = o.CB, AT = new IdName { Id = i.Id, Name = i.FullName } })
                     .Select(x => new TicketModel
                     {
                         Id = x.T.Id,
                         Name = x.T.Title,
                         Description = x.T.Description,
                         Priority =x.T.Priority,
                         ProjectId = x.T.Project,
                         Status = x.T.Status,
                         Type = x.T.Type,
                         CreatedDate = x.T.CreatedDate,
                         CreatedBy = x.CB,
                         AssignTo = x.AT
                         
                     }).ToList();
            }
        }
        public TicketModel GetTicket(int id)
        {
            using (var ctx = CreateContext())
            {
                var item = ctx.tblTickets.FirstOrDefault(x => x.Id == id);
                if (item != null)
                {
                    var createdBy = ctx.tblAccounts
                        .FirstOrDefault(x => x.Id == item.CreatedBy);
                    var assignTo = ctx.tblAccounts
                        .FirstOrDefault(x => x.Id == item.AssignTo);
                    return new TicketModel
                    {
                        Id = item.Id,
                        ProjectId = item.Project,
                        Name = item.Title,
                        Description = item.Description,
                        Priority = item.Priority,
                        Status = item.Status,
                        Type = item.Type,
                        CreatedDate = item.CreatedDate,
                        CreatedBy = createdBy == null ? null : new IdName {  Id = createdBy.Id, Name = createdBy.FullName },
                        AssignTo = assignTo == null ? null : new IdName { Id = assignTo.Id, Name = assignTo.FullName },
                    };
                }
                return new TicketModel();
            }
        }
        public int UpdateTicket(TicketModel model)
        {
            using (var ctx = CreateContext())
            {
                var item = ctx.tblTickets.FirstOrDefault(x => x.Id == model.Id);
                if (item == null)
                {
                    item = new Data.tblTicket();
                    item.CreatedDate = DateTime.Now;
                    item.CreatedBy = model.CreatedBy.Id;
                    ctx.tblTickets.InsertOnSubmit(item);
                }
                item.Project = model.ProjectId;
                item.Title = model.Name;
                item.Description = model.Description;
                item.Priority = model.Priority;
                item.Status = model.Status;
                item.Type = model.Type;
                item.AssignTo = model.AssignTo.Id;
                
                ctx.SubmitChanges();
                return item.Id;
            }
        }
        public void DeleteTicket(int id)
        {
            using (var ctx = CreateContext())
            {
                var item = ctx.tblTickets.FirstOrDefault(x => x.Id == id);
                if (item != null)
                {
                    ctx.tblTickets.DeleteOnSubmit(item);
                    ctx.SubmitChanges();
                }
            }
        }
        #endregion;
    }
}
