﻿using InetecTracker.Infrastructure.Interfaces;
using InetecTracker.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InetecTracker.Services.Developer
{
    public class DeveloperService : Base.BaseService, IDeveloperService
    {


        #region Ticket CRUD
        public List<TicketModel> GetTickets(int projectId)
        {
            using (var ctx = CreateContext())
            {
                return ctx.tblTickets
                    .Where(x => x.Project == projectId)
                     .Join(ctx.tblAccounts, o => o.CreatedBy, i => i.Id, (o, i) => new { T = o, CB = new IdName { Id = i.Id, Name = i.FullName } })
                     .Join(ctx.tblAccounts, o => o.T.AssignTo, i => i.Id, (o, i) => new { T = o.T, CB = o.CB, AT = new IdName { Id = i.Id, Name = i.FullName } })
                     .Select(x => new TicketModel
                     {
                         Id = x.T.Id,
                         Name = x.T.Title,
                         Description = x.T.Description,
                         Priority = x.T.Priority,
                         ProjectId = x.T.Project,
                         Status = x.T.Status,
                         Type = x.T.Type,
                         CreatedDate = x.T.CreatedDate,
                         CreatedBy = x.CB,
                         AssignTo = x.AT

                     }).ToList();
            }
        }

        public TicketModel GetTicket(int id)
        {
            using (var ctx = CreateContext())
            {
                var item = ctx.tblTickets.FirstOrDefault(x => x.Id == id);
                if (item != null)
                {
                    var createdBy = ctx.tblAccounts
                        .FirstOrDefault(x => x.Id == item.CreatedBy);
                    var assignTo = ctx.tblAccounts
                        .FirstOrDefault(x => x.Id == item.AssignTo);

                    return new TicketModel
                    {
                        Id = item.Id,
                        ProjectId = item.Project,
                        Name = item.Title,
                        Description = item.Description,
                        Priority = item.Priority,
                        Status = item.Status,
                        Type = item.Type,
                        CreatedDate = item.CreatedDate,
                        CreatedBy = createdBy == null ? null : new IdName { Id = createdBy.Id, Name = createdBy.FullName },
                        AssignTo = assignTo == null ? null : new IdName { Id = assignTo.Id, Name = assignTo.FullName },
                    };
                }
                return new TicketModel();
            }
        }

        public int UpdateTicket(TicketModel model)
        {
            using (var ctx = CreateContext())
            {
                var item = ctx.tblTickets.FirstOrDefault(x => x.Id == model.Id);
                if (item == null)
                {
                    item = new Data.tblTicket();
                    item.CreatedDate = DateTime.Now;
                    item.CreatedBy = model.CreatedBy.Id;
                    item.Project = model.ProjectId;
                    ctx.tblTickets.InsertOnSubmit(item);
                }
               
                item.Title = model.Name;
                item.Description = model.Description;
                item.Priority = model.Priority;
                item.Status = model.Status;
                item.Type = model.Type;
                item.AssignTo = model.AssignTo.Id;

                ctx.SubmitChanges();
                return item.Id;
            }
        }

        public void DeleteTicket(int id)
        {
            throw new NotImplementedException();
        }
        #endregion;


        public List<IdName> GetAccountsByProject(int projectId)
        {
            using (var ctx = CreateContext())
            {
                return
                    ctx.tblProject_Members.Where(x => x.Project == projectId)
                    .Join(ctx.tblAccounts.Where(x => x.IsApproved && !x.IsDeleted && !x.IsLockedOut),
                    o => o.Account,
                    i => i.Id,
                    (o, i) => new IdName { Id = i.Id, Name = String.Format("{0} : {1}", i.Email, i.FullName) })
                    .ToList();
            }
        }

        public List<IdName> GetProjects(int userId)
        {
            using (var ctx = CreateContext())
            {
                return ctx.tblProject_Members.Where(x => x.Account == userId)
                    .Join(ctx.tblProjects, o => o.Project, i => i.Id, (o, i) => new IdName { Id = i.Id, Name = i.Title })
                    .ToList();
            }
        }

    }
}
