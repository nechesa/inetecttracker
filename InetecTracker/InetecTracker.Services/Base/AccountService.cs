﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Threading.Tasks;
using InetecTracker.Infrastructure;
using InetecTracker.Infrastructure.Interfaces;
using InetecTracker.Infrastructure.Models;
using System.Web.Security;

namespace InetecTracker.Services.Base
{
    public class AccountService : Base.BaseService, IAccountService, IDisposable
    {

        #region private 

        private AccountModel GetAccountById(int id)
        {
            using (var ctx = CreateContext())
            {
                var account = ctx.tblAccounts.FirstOrDefault(x => x.Id == id);
                if (account != null)
                {
                    return new AccountModel
                    {
                        Id = account.Id,
                        AccountType = account.AccountType,
                        Email = account.Email,
                        FullName = account.FullName,
                        IsApproved = account.IsApproved,
                        IsLockedOut = account.IsLockedOut,
                        PasswordHash = account.PasswordHash,
                        PasswordSalt = account.PasswordSalt

                    };
                }
            }
            return null;
        }

        private AccountModel GetAccountByEmail(string email)
        {
            using (var ctx = CreateContext())
            {
                var account = ctx.tblAccounts.FirstOrDefault(x => x.Email == email);
                if (account != null)
                {
                    return new AccountModel
                    {
                        Id = account.Id,
                        AccountType = account.AccountType,
                        Email = account.Email,
                        FullName = account.FullName,
                        IsApproved = account.IsApproved,
                        IsLockedOut = account.IsLockedOut,
                        PasswordHash = account.PasswordHash,
                        PasswordSalt = account.PasswordSalt
                    };
                }
            }
            return null;
        }

        #endregion;

        public List<AccountModel> GetAllAccounts()
        {
            using (var ctx = CreateContext())
            {
                return ctx.tblAccounts.Select(x => new AccountModel
                {
                    Id = x.Id,
                    AccountType = x.AccountType,
                    Email = x.Email,
                    FullName = x.FullName,
                    IsApproved = x.IsApproved,
                    IsLockedOut = x.IsLockedOut,
                    PasswordHash = x.PasswordHash,
                    PasswordSalt = x.PasswordSalt
                })
                .ToList();
            }
        }
        public void CreateAccount(AccountModel model, string password)
        {
            string passwordSalt = GenerateSalt();
            string passwordHash = EncodePassword(password, passwordSalt);


            using (var ctx = CreateContext())
            {
                ctx.tblAccounts.InsertOnSubmit(new Data.tblAccount
                {
                    AccountType = model.AccountType,
                    Email = model.Email,
                    FullName = model.FullName,
                    IsApproved = model.IsApproved,
                    IsDeleted = false,
                    IsLockedOut = false,
                    PasswordHash = passwordHash,
                    PasswordSalt = passwordSalt,
                    CretaeDate = DateTime.Now
                });
                ctx.SubmitChanges();
            }
        }

        public int GetAccountId(string email)
        {
            AccountModel account = GetAccountByEmail(email);
            if (account != null)
            {
                return account.Id;
            }
            return 0;
        }

        public string GetAccountName(int id)
        {
            AccountModel account = GetAccountById(id);
            if (account != null)
            {
                return account.FullName;
            }
            return String.Empty;
        }

        public int GetAccountRole(string email)
        {
            AccountModel account = GetAccountByEmail(email);
            if (account != null)
            {
                return account.AccountType;
            }
            return (int)AccountType.Anonymous;
        }

        public bool IsAccountActive(string email)
        {
            AccountModel account = GetAccountByEmail(email);

            if (account != null)
            {
                return account.IsApproved && !account.IsDeleted && !account.IsLockedOut;
            }

            return false;
        }

        public bool IsAccountExist(int id)
        {
            AccountModel account = GetAccountById(id);
            return account != null;
        }

        public bool IsAccountLockedOrDeleted(string email)
        {
            AccountModel account = GetAccountByEmail(email);
            if (account != null)
            {
                return account.IsLockedOut;
            }
            return true;
        }

        public bool IsEmailExist(string email)
        {
            AccountModel account = GetAccountByEmail(email);
            return account != null;
        }

        public void ChangePassword(string newPassword, int accountId)
        {
            using (var ctx = CreateContext())
            {
                var item = ctx.tblAccounts.FirstOrDefault(x => x.Id == accountId);
                if (item != null)
                {
                    string newSalt = GenerateSalt();
                    string newHash = EncodePassword(newPassword, newSalt);

                    item.PasswordSalt = newSalt;
                    item.PasswordHash = newHash;

                    ctx.SubmitChanges();
                }
            }
        }
        public bool IsPasswordValid(LoginModel model)
        {
            AccountModel account = GetAccountByEmail(model.Email);
            if (account != null)
            {
                if (String.Compare(account.PasswordHash, EncodePassword(model.Password, account.PasswordSalt), StringComparison.CurrentCultureIgnoreCase) == 0)
                {
                    return true;
                }
            }
            return false;
        }

        public void SignIn(LoginModel model, out int role)
        {
            role = (int)AccountType.Anonymous;

            AccountModel account = GetAccountByEmail(model.Email);
            if (account != null)
            {
                role = account.AccountType;
                
                FormsAuthentication.SetAuthCookie(model.Email, false);
            }
        }

        public void SignOut()
        {
            //string userName = HttpContext.Current.User.Identity.Name;
            FormsAuthentication.SignOut();
        }

        

        #region password encryption

        public string EncodePassword(string password, string salt)
        {
            byte[] partOne = Encoding.Unicode.GetBytes(password);
            byte[] partTwo = Convert.FromBase64String(salt);
            byte[] total = new byte[partTwo.Length + partOne.Length];
            byte[] result = null;

            Buffer.BlockCopy(partTwo, 0, total, 0, partTwo.Length);
            Buffer.BlockCopy(partOne, 0, total, partTwo.Length, partOne.Length);

            using (HashAlgorithm s = new MD5CryptoServiceProvider())
            {
                result = s.ComputeHash(total);
            }

            return Convert.ToBase64String(result);
        }

        public string GenerateSalt()
        {
            byte[] buf = new byte[16];

            using (RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider())
            {
                rng.GetBytes(buf);
            }

            return Convert.ToBase64String(buf);
        }

        #endregion


        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~InitService() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}
