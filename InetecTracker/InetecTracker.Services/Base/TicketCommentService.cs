﻿using InetecTracker.Data;
using InetecTracker.Infrastructure.Interfaces;
using InetecTracker.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InetecTracker.Services.Base
{
    public class TicketCommentService : BaseService, ITicketCommentService
    {
        public List<TicketCommentModel> GetTicketComments(int ticketId)
        {
            using (var ctx = CreateContext())
            {
                return ctx.tblTicket_Comments.Where(x => x.Ticket == ticketId)
                      .Join(ctx.tblAccounts, o => o.CreatedBy, i => i.Id, (o, i) => new { T = o, A = new IdName { Id = i.Id, Name = i.FullName } })
                      .Select(x => new TicketCommentModel
                      {
                          Id = x.T.Id,
                          Comment = x.T.Comment,
                          CreateDate = x.T.CreatedDate,
                          CreateBy = x.A
                      })
                      .ToList();
            }
        }

        public int AddTicketComment(TicketCommentModel model)
        {
            using (var ctx = CreateContext())
            {
                tblTicket_Comment item = new tblTicket_Comment
                {
                     Ticket = model.TicketId,
                     Comment = model.Comment,
                     CreatedDate = DateTime.Now,
                     CreatedBy = model.CreateBy.Id
                };
                ctx.tblTicket_Comments.InsertOnSubmit(item);
                ctx.SubmitChanges();

                return item.Id;
            }
        }

        public void RemoveTicketComment(int id)
        {
            using (var ctx = CreateContext())
            {
                var item = ctx.tblTicket_Comments.FirstOrDefault(x => x.Id == id);
                if (item != null)
                {
                    ctx.tblTicket_Comments.DeleteOnSubmit(item);
                    ctx.SubmitChanges();
                }
            }
        }

        public bool IsCommentAvailable(int ticketId, int userId)
        {
            using (var ctx = CreateContext())
            {
                var item = ctx.tblTickets.FirstOrDefault(x => x.Id == ticketId);
                if (item != null)
                {
                    return ctx.tblProject_Members.Any(x => x.Project == item.Project && x.Account == userId);
                }

                return false;
            }
        }
    }
}
