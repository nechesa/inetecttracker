﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InetecTracker.Data;


namespace InetecTracker.Services.Base
{
    public class BaseService
    {
        private string _connectionStr;
        protected string connectionStr
        {
            get
            {
                if (String.IsNullOrWhiteSpace(_connectionStr))
                    _connectionStr = ConfigurationManager.ConnectionStrings["InetecConnectionString"].ConnectionString;
                return _connectionStr;
            }
        }



        //private InetecTrackerDataClassesDataContext _dataCtx;

        //protected InetecTrackerDataClassesDataContext dataCtx
        //{
        //    get
        //    {
        //        if (_dataCtx == null)
        //            _dataCtx = new InetecTrackerDataClassesDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["InetecConnectionString"].ConnectionString);
        //        return _dataCtx;
        //    }
        //}


        protected InetecTrackerDataClassesDataContext CreateContext()
        {
            return new InetecTrackerDataClassesDataContext(connectionStr);
        }
    }
}
